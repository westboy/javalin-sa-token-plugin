package org.dromara.javalin;

import cn.dev33.satoken.context.SaTokenContext;
import cn.dev33.satoken.context.model.SaRequest;
import cn.dev33.satoken.context.model.SaResponse;
import cn.dev33.satoken.context.model.SaStorage;
import cn.hutool.core.text.AntPathMatcher;
import io.javalin.http.Context;
import org.dromara.javalin.servlet.SaRequestForServlet;
import org.dromara.javalin.servlet.SaResponseForServlet;
import org.dromara.javalin.servlet.SaStorageForServlet;


public class SaTokenContextImpl implements SaTokenContext {

    private static final AntPathMatcher matcher = new AntPathMatcher();
    private final Context ctx;


    public SaTokenContextImpl(Context ctx) {
        this.ctx = ctx;
    }

    /**
     * 获取当前请求的Request对象
     */
    @Override
    public SaRequest getRequest() {
        return new SaRequestForServlet(ctx.req());
    }

    /**
     * 获取当前请求的Response对象
     */
    @Override
    public SaResponse getResponse() {
        return new SaResponseForServlet(ctx.res());
    }

    /**
     * 获取当前请求的 [存储器] 对象
     */
    @Override
    public SaStorage getStorage() {
        return new SaStorageForServlet(ctx.req());
    }

    /**
     * 校验指定路由匹配符是否可以匹配成功指定路径
     */
    @Override
    public boolean matchPath(String pattern, String path) {
        return matcher.match(pattern,path);
    }

}

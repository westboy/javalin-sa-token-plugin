package org.dromara.javalin;

import cn.dev33.satoken.config.SaTokenConfig;

public class SaTokenJavalinPluginConfig extends SaTokenConfig {

    private String scanPackage;

    protected SaTokenJavalinPluginConfig() {
    }

    public String getScanPackage() {
        return scanPackage;
    }

    public void setScanPackage(String scanPackage) {
        this.scanPackage = scanPackage;
    }

}

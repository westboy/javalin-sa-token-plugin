package org.dromara.javalin.handler;

import cn.dev33.satoken.SaManager;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.router.JavalinDefaultRouting;
import org.dromara.javalin.SaTokenContextImpl;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class SaTokenContextHandler implements Handler, Consumer<JavalinDefaultRouting> {

    @Override
    public void handle(@NotNull Context context) {
        SaManager.setSaTokenContext(new SaTokenContextImpl(context));
    }

    @Override
    public void accept(JavalinDefaultRouting javalinDefaultRouting) {
        javalinDefaultRouting.before(this);
    }
}
